# -*- coding: utf-8 -*-
#!/usr/bin/env python
#Database classes
from google.appengine.ext import db

class CustomPages(db.Model):
	published = db.BooleanProperty()
	header = db.StringProperty()
	url = db.StringProperty()
	content = db.TextProperty()
	date = db.DateProperty()

class Cats(db.Model):
	name = db.StringProperty()
	link = db.StringProperty()

class Image(db.Model):
	flickrid = db.StringProperty()
	post = db.IntegerProperty()
	preview = db.StringProperty()
	full = db.StringProperty()
	width = db.StringProperty() #preview width
	last = db.DateProperty()
	
class Entry(db.Model):
	published = db.BooleanProperty()
	header = db.StringProperty()
	rawcontent = db.TextProperty()
	content = db.TextProperty()
	shortcontent = db.TextProperty()
	category = db.IntegerProperty()
	date = db.DateProperty()

class Settings(db.Model):
	name = db.StringProperty()
	val = db.StringProperty()
