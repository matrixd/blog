#!/usr/bin/python
# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-
import md5
import binascii
import urllib2

def getThumb(photoId, size, obj):
	
	sw = "%sapi_key%sauth_token%smethodflickr.photos.getSizesphoto_id%s" % (obj['consumer_secret'], obj['consumer_key'], obj['auth_token'], photoId)
	raw = md5.new(sw).digest()
	signature = binascii.hexlify(raw)
	
	url = "http://flickr.com/services/rest/?method=flickr.photos.getSizes&api_key=%s&auth_token=%s&photo_id=%s&api_sig=%s" % (obj['consumer_key'], obj['auth_token'], photoId, signature)
	resp = urllib2.urlopen(url)
	
	thumbs = {}
	for line in resp:
		if '<size label="' in line:
			def get(statement):
				s = line.index(statement) + len(statement)
				end = line[s:].index('"') + s
				return line[s:end]
			
			width = get('width="')
			if get('<size label="') == 'Original':
			    height = get('height="')
			    url = get('source="')
			    values = {"url": url, "width": width, "height": height}
			    thumbs['original'] = values
		    if width == size:
		        height = get('height="')
			    url = get('source="')
			    values = {"url": url, "width": width, "height": height}
			    thumbs[width] = values
	return thumbs
