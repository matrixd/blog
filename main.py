# -*- coding: utf-8 -*-
#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import cgi
import webapp2
import jinja2
import os
import datetime
import dbClasses
import flickr

from google.appengine.api import users
from google.appengine.ext import db


#global vars

jinja_environment = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)))

cats = {}
settings = {}

#Functions
 
def pagesKey(var):
	return db.Key.from_path('url', var)
	
def isNumber(s):
    try:
        int(s)
        return True
    except ValueError:
        return False

def navbar():
	return settings['navbar']
	
def getCats():
	global cats
	cats = {}
	tmp = db.GqlQuery("SELECT * FROM Cats")
	for c in tmp:
		 cats[c.key().id()] = c

def getSettings():
	global settings
	settings = {}
	tmp = db.GqlQuery("SELECT * FROM Settings")
	for s in tmp:
		settings[s.name] = s.val
	settings['navbar'] = """
<div class="navbar navbar-fixed-top">
	  <div class="navbar-inner">
		<ul class="nav">
		  <li><a href="/">На главную</a></li>
		  <li><a href="/contacts">Контакты</a></li>
		  <li><a href="/about">Обо мне</a></li>
		  <li class="special"><a href="http://www.flickr.com/photos/matrixd/">Мое фото</a></li>
		</ul>
	</div>
</div>
	""".decode('utf-8')

getCats()
getSettings()

#classes used to display pages avaible for user

class MainPage(webapp2.RequestHandler):
	def get(self):
		result = db.GqlQuery("SELECT * From Entry ORDER BY date DESC")
		template = jinja_environment.get_template('index.html')
		template_values = {
			'result': result,
			'navbar': navbar(),
			'cats': cats
		}
		self.response.write(template.render(template_values))

class PageHandler(webapp2.RequestHandler):
	def get(self, url):
		if isNumber(url):
			res = Entry.get_by_id(int(url))
			if res:
				template = jinja_environment.get_template('article.html')
				template_values = {
					'res': res,
					'navbar': navbar(),
					'catn': cats[res.category].name,
					'catl': cats[res.category].link
				}
				self.response.write(template.render(template_values))
			else:
				f = open("404.html")
				self.response.write(f.read())
		else:
			res = db.GqlQuery("SELECT * FROM CustomPages WHERE url = '%s'" %url)
			if res.count()>0:
				template = jinja_environment.get_template('custom.html')
				template_values = {
					'res': res[0],
					'navbar': navbar()
				}
				self.response.write(template.render(template_values))
			else:
				f = open("404.html")
				self.response.write(f.read())
			
class CatHandler(webapp2.RequestHandler):
	def get(self, url):
		re = 0
		name = 'a'
		for key, cat in cats.iteritems():
			if cat.link == url:
				global re, name
				re = key
				name = cat.name
				break
		result = db.GqlQuery("SELECT * FROM Entry WHERE category = %d" %re)
		template = jinja_environment.get_template('cat.html')
		template_values = {
			'result': result,
			'navbar': navbar(),
			'name': name,
			'count': result.count(),
			'cats': cats
		}
		self.response.write(template.render(template_values))

#admin pages etc

class AdminAdd(webapp2.RequestHandler):
	def get(self):
		user = users.get_current_user()
		if user:
			if users.is_current_user_admin():
				self.response.write("""<html><head></head><body>Hello, %s <a href="%s">logout</a></body></html>""" %(user.nickname(), users.create_logout_url(self.request.uri)))
				template = jinja_environment.get_template('add.html')
				template_values = {
					'cats': cats
				}
				self.response.write(template.render(template_values))
			else:
				self.response.write("""sad! <a href="%s">Login again</a>""" % users.create_login_url(self.request.uri))
		else:
			self.redirect(users.create_login_url(self.request.uri))
	
class AdminSave(webapp2.RequestHandler):
	def post(self):
		user = users.get_current_user()
		if user:
			if users.is_current_user_admin():
				if self.request.get('id') == "new":
					entry = Entry()
					if self.request.get('published') == 1:
						entry.published = True
					else:
						entry.published = False
					entry.header = self.request.get('header')
					entry.content = db.Text(self.request.get('content'))
					entry.shortcontent = db.Text(self.request.get('shortcontent'))
					entry.category = int(self.request.get('category'))
					month,day,year = self.request.get('date').split('/')
					entry.date = datetime.date(int(year), int(month), int(day))
					entry.put()
					self.redirect("/admin/edit/%s" % entry.key().id())
				else:
					entry = Entry.get_by_id(int(self.request.get('id')))
					entry.header = self.request.get('header')
					entry.content = db.Text(self.request.get('content'))
					entry.category = int(self.request.get('category'))
					month,day,year = self.request.get('date').split('/')
					entry.date = datetime.date(int(year), int(month), int(day))
					entry.put()
					self.redirect("/admin/edit/%s" % entry.key().id())
			else:
				self.response.write("""sad! <a href="%s">Login again</a>""" % users.create_login_url(self.request.uri))
		else:
			self.redirect(users.create_login_url(self.request.uri))
		
class Admin(webapp2.RequestHandler):
	def get(self):
		user = users.get_current_user()
		if user:
			if users.is_current_user_admin():
				result = db.GqlQuery("SELECT * From Entry")
				template = jinja_environment.get_template('admin.html')
				template_values = {
					'result': result
				}
				self.response.write(template.render(template_values))
			else:
				self.response.write("""sad! <a href="%s">Login again</a>""" % users.create_login_url(self.request.uri))
		else:
			self.redirect(users.create_login_url(self.request.uri))
		
class AdminEdit(webapp2.RequestHandler):
	def get(self, post_id):
		user = users.get_current_user()
		if user:
			if users.is_current_user_admin():
				res = Entry.get_by_id(int(post_id))
				template = jinja_environment.get_template('edit.html')
				date = "%02d/%02d/%04d" % (res.date.month, res.date.day, res.date.year) 
				template_values = {
					'res': res,
					'date': date,
					'cats': cats
				}
				self.response.write(template.render(template_values))
			else:
				self.response.write("""sad! <a href="%s">Login again</a>""" % users.create_login_url(self.request.uri))
		else:
			self.redirect(users.create_login_url(self.request.uri))	
		
class AdminCats(webapp2.RequestHandler):
	def get(self):
		user = users.get_current_user()
		if user:
			if users.is_current_user_admin():
				template = jinja_environment.get_template('cats.html')
				template_values = {
					'cats': cats
				}
				self.response.write(template.render(template_values))
			else:
				self.response.write("""sad! <a href="%s">Login again</a>""" % users.create_login_url(self.request.uri))
		else:
			self.redirect(users.create_login_url(self.request.uri))

	def post(self):
		user = users.get_current_user()
		if user:
			if users.is_current_user_admin():
				if self.request.get('action') == "add":
					newc = Cats()
					newc.name = self.request.get('newName')
					newc.link = self.request.get('newLink')
					newc.put()
					getCats()
				if self.request.get('action') == "del":
					cats[int(self.request.get('key'))].delete()
					getCats()
				if self.request.get('action') == "rename":
					curc = cats[int(self.request.get('key'))]
					curc.name = self.request.get('name')
					curc.put()
					getCats()
				if self.request.get('action') == "relink":
					curc = cats[int(self.request.get('key'))]
					curc.link = self.request.get('link')
					curc.put()
					getCats()
				self.redirect("/admin/cats")
			else:
				self.response.write("""sad! <a href="%s">Login again</a>""" % users.create_login_url(self.request.uri))
		else:
			self.redirect(users.create_login_url(self.request.uri))
		
class AdminAddPage(webapp2.RequestHandler):
	def get(self):
		user = users.get_current_user()
		if user:
			if users.is_current_user_admin():
				#if url == 'new':
				#	template = jinja_environment.get_template('addpage.html')
				#	self.response.write(template.render())
				#else:
				#	res = CustomPages.get_by_id(int(url))
				#	template = jinja_environment.get_template('pageEdit.html')
				#	date = "%02d/%02d/%04d" % (res.date.month, res.date.day, res.date.year) 
				#	template_values = {
				#		'res': res,
				#		'date': date
				#	}
				#	self.response.write(template.render(template_values))
				template = jinja_environment.get_template('addpage.html')
				self.response.write(template.render())
			else:
				self.response.write("""sad! <a href="%s">Login again</a>""" % users.create_login_url(self.request.uri))
		else:
			self.redirect(users.create_login_url(self.request.uri))
			
	def post(self):
		user = users.get_current_user()
		if user:
			if users.is_current_user_admin():
				page = CustomPages()
				if self.request.get('published') == 1:
					page.published = True
				else:
					page.published = False
				page.header = self.request.get('header')
				page.url = self.request.get('link')
				page.content = db.Text(self.request.get('content'))
				month,day,year = self.request.get('date').split('/')
				page.date = datetime.date(int(year), int(month), int(day))
				page.put()						
				self.redirect("/admin/pageEdit/%s" % page.key().id())					
			else:
				self.response.write("""sad! <a href="%s">Login again</a>""" % users.create_login_url(self.request.uri))
		else:
			self.redirect(users.create_login_url(self.request.uri))
			
class AdminPage(webapp2.RequestHandler):
	def get(self):
		user = users.get_current_user()
		if user:
			if users.is_current_user_admin():
				result = db.GqlQuery("SELECT * From CustomPages ORDER BY date DESC")
				template = jinja_environment.get_template('pages.html')
				template_values = {
					'result': result
				}
				self.response.write(template.render(template_values))
			else:
				self.response.write("""sad! <a href="%s">Login again</a>""" % users.create_login_url(self.request.uri))
		else:
			self.redirect(users.create_login_url(self.request.uri))
			
class AdminPageEdit(webapp2.RequestHandler):
	def get(self, num):
		user = users.get_current_user()
		if user:
			if users.is_current_user_admin():
				res = CustomPages.get_by_id(int(num))
				template = jinja_environment.get_template('pageEdit.html')
				template_values = {
					'res': res
				}
				self.response.write(template.render(template_values))
			else:
				self.response.write("""sad! <a href="%s">Login again</a>""" % users.create_login_url(self.request.uri))
		else:
			self.redirect(users.create_login_url(self.request.uri))
			
	def post(self, num):
		user = users.get_current_user()
		if user:
			if users.is_current_user_admin():
				page = CustomPages.get_by_id(int(self.request.get('id')))
				page.header = self.request.get('header')
				page.content = db.Text(self.request.get('content'))
				page.url = self.request.get('link')
				page.put()
				self.redirect("/admin/pageEdit/%s" % self.request.get('id'))
			else:
				self.response.write("""sad! <a href="%s">Login again</a>""" % users.create_login_url(self.request.uri))
		else:
			self.redirect(users.create_login_url(self.request.uri))

class AdminSettings(webapp2.RequestHandler):
	def get(self):
		user = users.get_current_user()
		if user:
			if users.is_current_user_admin():
				res = db.GqlQuery("SELECT * From Settings")
				template = jinja_environment.get_template('settings.html')
				template_values = {
					'res': res
				}
				self.response.write(template.render(template_values))
			else:
				self.response.write("""sad! <a href="%s">Login again</a>""" % users.create_login_url(self.request.uri))
		else:
			self.redirect(users.create_login_url(self.request.uri))
	def post(self):
		user = users.get_current_user()
		if user:
			if users.is_current_user_admin():
				if self.request.get('action') == "new":
					s = dbClasses.Settings()
					s.name = self.request.get('name')
					s.val = self.request.get('val')
					s.put()					
				getSettings()
				self.redirect("/admin/settings")
			else:
				self.response.write("""sad! <a href="%s">Login again</a>""" % users.create_login_url(self.request.uri))
		else:
			self.redirect(users.create_login_url(self.request.uri))

class PageParser(webapp2.RequestHandler):
	def post(self):
		user = users.get_current_user()
		if user:
			if users.is_current_user_admin():
				content = self.request.get('content')
				post = content.find("\n")
				firstLine = content.find("\n", post+1)
				secondLine = content.find("\n", firstLine+1)
				
				#postId = int(content[13:post])
				postTitle = content[post+1:firstLine]
				year, month, day = content[firstLine+1:secondLine].split('-')
				#postContent = content[secondLine+1:]
				entry = Entry()
				#working with post content
				#searching images
				
				#getting content
				contentEndLine = content.find("<---ENDC--->", secondLine)
				shortContentBegin = contentEndLine+len("<---ENDC--->")
				entry.content = db.Text(content[secondLine+1:contentEndLine])
				
				#getting shortcat
				shortContentEnd = content.find("<---ENDSHC--->")
				entry.shortcontent = db.Text(content[shortContentBegin:shortContentEnd])
				
				#adding entry to the blog			
				entry.header = postTitle
				entry.category = int(self.request.get('category'))
				entry.date = datetime.date(int(year), int(month), int(day))
				entry.put()
				self.redirect("/admin/edit/%s" % entry.key().id())
			else:
				self.response.write("""sad! <a href="%s">Login again</a>""" % users.create_login_url(self.request.uri))
		else:
			self.redir1ect(users.create_login_url(self.request.uri))
	def get(self):
		user = users.get_current_user()
		if user:
			if users.is_current_user_admin():
				self.response.write("""<html><head></head><body>Hello, %s <a href="%s">logout</a></body></html>""" %(user.nickname(), users.create_logout_url(self.request.uri)))
				template = jinja_environment.get_template('parse.html')
				template_values = {
					'cats': cats
				}
				self.response.write(template.render(template_values))
			else:
				self.response.write("""sad! <a href="%s">Login again</a>""" % users.create_login_url(self.request.uri))
		else:
			self.redirect(users.create_login_url(self.request.uri))


app = webapp2.WSGIApplication([('/', MainPage), 
								('/admin/add', AdminAdd), 
								('/admin/save', AdminSave), 
								('/admin/', Admin),
								('/admin/edit/(\d+)', AdminEdit),
								('/admin/cats', AdminCats),
								('/(\w+)', PageHandler),
								('/cat/(\w+)', CatHandler),
								('/admin/addpage', AdminAddPage),
								('/admin/pages', AdminPage),
								('/admin/pageEdit/(\d+)', AdminPageEdit),
								('/admin/parser', PageParser),
								('/admin/settings', AdminSettings),], debug=True)
